#install.packages("BGLR")
#install.packages("plyr")
library(BGLR)
library(plyr)

#setwd("~/Documents/wheat/Phenotypic Data/breedbase/genotyping/from_brian/NIFA/")

locs <- c("IL","OH","KY","IN")

#oh_phenotype_file <- "oh_blups.txt"
oh_phenotype_file <- "data/oh_blups.csv"
in_phenotype_file <- "data/in_blups.csv"
ky_phenotype_file <- "data/ky_blups.csv"
il_phenotype_file <- "data/il_blups.csv"
key_file <- "data/sample_keys.csv"
encoded_gt_zip <- "data/encoded_gt.zip"
encoded_gt <- "data/encoded_gt.txt"

list.files()

oh_pheno <- read.csv(oh_phenotype_file,sep=",",stringsAsFactors = F, header = T)
in_pheno <- read.csv(in_phenotype_file,sep="\t",stringsAsFactors = F, header = T)
ky_pheno <- read.csv(ky_phenotype_file,sep=",",stringsAsFactors = F, header = T)
il_pheno <- read.csv(il_phenotype_file,sep=",",stringsAsFactors = F,header=T)

head(oh_pheno)
head(in_pheno)
head(ky_pheno)
head(il_pheno)

## Resolve headers
# add prefix in header names
#name <- "carlos"; prefix <- "dr"; delimiter="."
add_prefix <- function(name, prefix, delimiter="."){
  paste0(c(prefix,name),collapse = ".")
}

colnames(oh_pheno) <- sapply(colnames(oh_pheno),add_prefix,"OH")
colnames(in_pheno) <- sapply(colnames(in_pheno),add_prefix,"IN")
colnames(ky_pheno) <- sapply(colnames(ky_pheno),add_prefix,"KY")
colnames(il_pheno) <- sapply(colnames(il_pheno),add_prefix,"IL")

# add prefix to accession names
oh_pheno[,"UNIQUE.NAME"] <- sapply(oh_pheno[,"OH.X"],add_prefix,"OH")
in_pheno[,"UNIQUE.NAME"] <- sapply(in_pheno[,"IN.Lines"],add_prefix,"IN")
ky_pheno[,"UNIQUE.NAME"] <- sapply(ky_pheno[,"KY.name"],add_prefix,"KY")
il_pheno[,"UNIQUE.NAME"] <- sapply(il_pheno[,"IL.sample_name"],add_prefix,"IL")

# make column to preserve name of all locations
oh_pheno[,"NAME"] <- oh_pheno[,"OH.X"]
in_pheno[,"NAME"] <- in_pheno[,"IN.Lines"]
ky_pheno[,"NAME"] <- ky_pheno[,"KY.name"]
il_pheno[,"NAME"] <- il_pheno[,"IL.sample_name"]

# Merge all data
merged <- rbind.fill(rbind.fill(rbind.fill(oh_pheno,in_pheno),ky_pheno),il_pheno)
colnames(merged)

# Make output names for file
kf <- read.csv(key_file,sep=",",stringsAsFactors = F,header=T)
kf$names_out <- kf$FullSampleName
oh.samples.idx <- kf$pi_or_coordinator == "Clay Sneller"
kf$names_out[oh.samples.idx] <- kf$sample_names_oh_corrected[oh.samples.idx]
head(kf)

# # Read and check encoded genotype file
unzip(zipfile=encoded_gt_zip, exdir="./data/")
enc <- read.table(encoded_gt,header=T,sep="\t",row.names = 1,comment.char = "%")
# save(enc,file="encoded_gt.Rds")
# load("encoded_gt.Rds")
head(enc[,1:14])
head(enc[,(dim(enc)[2]-13):dim(enc)[2]])
tail(enc[,1:14])
tail(enc[,(dim(enc)[2]-13):dim(enc)[2]])

# Extract and transpose GT information
gt_idx <- c(11:dim(enc)[2])
t.enc <- t(enc[gt_idx])
row.names(t.enc) <- NULL
head(t.enc)[,1:10]
# save(t.enc,file="transposed_encoded.Rds")
# load("transposed_encoded.Rds")

# Get sample names
con <- file(encoded_gt,"r")
first_line <- readLines(con,n=1)
close(con)
sample_names <- strsplit(first_line,"\t")[[1]][gt_idx+1]

## Trim dataset for training
# Remove phenotyped lines with no genotypes
pheno_with_gt <- merged[merged[,"NAME"] %in% sample_names,]

# Set list of traits to analyze
names(pheno_with_gt)
# (traits <- names(pheno_with_gt)[c(1:6,14:21,23:26,28,31:44)]) # with IL
(traits <- names(pheno_with_gt)[c(2,4,6,8,14:21,23:26,29:43)])
lapply(pheno_with_gt[,traits],class)

# Assign genotype matrix, this step is unnecessary lol, just lazy to change BGLR example code
X <- t.enc

# Make output data.frame
# out_df <- pheno_with_gt
out_df <- kf[kf$sample_names_oh_corrected %in% sample_names,c("sample_names_oh_corrected","names_out","pi_or_coordinator")]
pis <- unique(out_df$pi_or_coordinator)
out_df$state <- locs[match(out_df$pi_or_coordinator,pis)]
out_df$unique.name <- paste(out_df$state,out_df$sample_names_oh_corrected,sep=".")
out_df <- out_df[,c("sample_names_oh_corrected","names_out","state","unique.name")]
out_df <- out_df[,c("unique.name","sample_names_oh_corrected","names_out")]
names(out_df) <- c("unique.name","sample.name.old","sample.name.new")
head(out_df)

# Trim duplicates in input trait and match order with GT
trimDuplicates <- function(pheno_with_gt,trait="OH.YLD",samples_kept_=samples_kept){
  row.names(pheno_with_gt) <- NULL
  pheno_with_gt[match(samples_kept_, pheno_with_gt[,"NAME"]),]
}
trait <- "OH.YLD"

runBGLR <- function(trait, pheno_with_gt, out_df, fixed.effect=NULL, random.effect=NULL){
  df.y <- data.frame(unique.name = out_df$unique.name
                     ,sample.name.old = out_df$sample.name.old)
  df.y[,trait] <- pheno_with_gt[match(out_df$unique.name,pheno_with_gt$UNIQUE.NAME),trait]
  tmp <- df.y[!is.na(df.y[,trait]),]
  y <- as.numeric(tmp[match(sample_names,tmp$sample.name.old),trait])
  #df.y <- trimDuplicates(pheno_with_gt,trait,sample_names)
  #y <- as.numeric(df.y[,trait])
  
  
  # Run model
  ETA <- list(#list(~factor(studyName), data=il_pheno, model='FIXED'),
    list(X=X, model="BRR"))
  
  fm <- BGLR(y=y,
       response_type = "gaussian",
       ETA=ETA,
       verbose=TRUE)
  par(mfrow=c(2,2))
  
  #1# Estimated Marker Effects & posterior SDs
  bHat<- fm$ETA[[1]]$b
  SD.bHat<- fm$ETA[[1]]$SD.b
  plot(bHat^2, ylab='Estimated Squared-Marker Effect',
       type='o',cex=.5,col=4,main=paste(trait,'Marker Effects'))
  
  #2# Predictions# Total prediction
  yHat<-fm$yHat
  tmp2<-range(c((y[!is.na(y)]),yHat[!is.na(y)]))
  plot(yHat[!is.na(y)]~(y[!is.na(y)]),xlab=paste('Observed, n=',sum(!is.na(y)),', R=',round(cor(yHat[!is.na(y)],(y[!is.na(y)]))),sep = ""),ylab='Predicted',col=2
       ,xlim=tmp2,ylim=tmp2
       ,main=paste(trait,"Predicted vs. Observed")); abline(a=0,b=1,col=4,lwd=1, lty=2)
  
  # Just the genomic part
  gHat<-(X%*%fm$ETA[[1]]$b)[!is.na(y)]
  tmp3 <- (y[!is.na(y)]-mean(y,na.rm=T))/sd(y,na.rm = T)
  plot(gHat~tmp3,xlab=paste('Phenotype, n=',sum(!is.na(y)),sep = ""), ylab='Predicted Genomic Value',main=paste(trait,"Predicted Genomic Values vs. Phenotypes"),col=2)
  
  #3# Godness of fit and related statistics
  fm$fit
  fm$varE # compare to var(y)
  
  # Residual variance
  varE<-scan('varE.dat')
  plot(varE,type='o',col=2,cex=.5,ylab=expression(var[e]),main=paste(trait,"Residual Variance"));abline(h=fm$varE,col=4,lwd=1,lty=2);abline(v=fm$burnIn/fm$thin,col=4,lty=2)
  
  for(fn in c("ETA_1_varB.dat","mu.dat","varE.dat")){
    if (file.exists(fn)){
      #Delete file if it exists
      file.remove(fn)
    }
  }
  
  # Merge predicted values to the data frame
  out_df[,trait] <- yHat[match(out_df[,"sample.name.old"], sample_names)]
  return(out_df)
}

# Print graphs to pdf file
pdf("output/BGLR_output.pdf",paper = "USr")

# Run BGLR for all traits and write predictions to csv file
for(trait in traits){
  print(paste("Running BGLR for",trait))
  out_df <- tryCatch(runBGLR(trait, pheno_with_gt, out_df),error=function(e){cat("Error running lmer for",trait,":",conditionMessage(e), "\n"); return(out_df)})
  write.csv(out_df[!duplicated(out_df$unique.name),],"output/output_predictions.csv",row.names = F)
}

dev.off()

# Write input phenotypes
first_cols_idx <- c(grep("UNIQUE.NAME",colnames(pheno_with_gt),ignore.case = T),grep("^NAME$",colnames(pheno_with_gt),ignore.case = T))
out_pheno_df <- pheno_with_gt[,c(first_cols_idx,setdiff(1:length(pheno_with_gt),first_cols_idx))]
write.table(out_pheno_df,"output/input_pheno.csv",row.names = F)


# # check corellation between traits
# out_df <- read.csv("output_predictions.csv")
# traits
# for(i in 4:8){
#   for(j in 4:8){
#     print(paste(colnames(out_df)[i],"x",colnames(out_df)[j],":",cor(out_df[,i],out_df[,j])))
#   }
# }
# 
# gs_out <- data.frame(sample_name = sample_names, gs_prediction = fm$yHat)
# gs_out[grep("OH15-191-52",gs_out[,1],ignore.case = T),]
# asc <- gs_out[order(gs_out$gs_prediction),"sample_name"]
# out_df[match(asc,out_df$sample.name.old),4]
# oh_pheno[match(asc,oh_pheno$OH.NAME),trait]
# pred <- gs_out[match(oh_pheno$OH.NAME,gs_out$sample_name),2]
# na.pred.idx <- is.na(pred)
# na.trait.idx <- is.na(oh_pheno[,trait])
# na.both.idx <- as.logical((!na.pred.idx) * (!na.trait.idx))
# na.both.idx
# cor(pred[na.both.idx],oh_pheno[na.both.idx,trait])
# head(oh_pheno)
