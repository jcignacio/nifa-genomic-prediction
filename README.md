# NIFA Project Repository

This is a repository for the NIFA Project between the wheat breeding programs in University of Illinois Urbana Champaign, University of Kentucky, Purdue University and the Ohio State University.

## Installation

Use the git clone to download input files and scripts

```bash
git clone https://jcignacio@bitbucket.org/jcignacio/nifa-genomic-prediction.git
```

## Genomic Prediction script usage

Prerequisites

- R
- R package BGLR
- R package plyr

```bash
Rscript scripts/run_bglr.R
```

## License
Not licensed yet